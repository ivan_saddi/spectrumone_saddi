from django.db import models

# Create your models here.
class Users(models.Model):
    email = models.CharField(max_length=100)
    password = models.CharField(max_length=64)
    firstname = models.CharField(max_length=75, null=True)
    lastname = models.CharField(max_length=50, null=True)
    confirmation = models.DateTimeField(default=None, null=True)