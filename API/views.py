from django.http import HttpResponse
from django.views import View
from .models import Users
import json, traceback, jwt, smtplib, datetime
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

# Create your views here.
class VerifyAccountHandler(View):
    def get(self, request):
        try:
            activation_key = request.GET.get("key")
            #decrypt jwt
            key_data = jwt.decode(activation_key, "s3cr3t_key_S1")
            user = Users.objects.get(email=str(key_data["email"]))
            date_now = datetime.datetime.now()
            if not user.confirmation:
                user.confirmation = date_now
            else:
                raise Exception()
        
        except jwt.exceptions.DecodeError:
            return HttpResponse(json.dumps({"success":False, "error_msg": "Invalid Activation Key"}), content_type="application/json")
        except Exception:
            return HttpResponse(json.dumps({"success":False, "error_msg": "User already activated"}), content_type="application/json")
        except:
            traceback.print_exc()
            return HttpResponse(json.dumps({"success":False}), content_type="application/json")
        else:
            user.save()
            return_data = {
            "user_id": user.id,
            "email": user.email,
            "confirmation": user.confirmation.strftime("%Y-%m-%d %H:%M:%S"), 
            }
            return HttpResponse(json.dumps({"success":True, "data": return_data}), content_type="application/json")

class AuthenticationHandler(View):
    def post(self, request):
        try:
            #get JSON POST data
            json_data = json.loads(request.body.decode("utf-8"))
            email = json_data["email"]
            password = json_data["password"]
            #check email-password tandem
            user = Users.objects.filter(email=email, password=password)
            
            if user.count() > 0 and user[0].confirmation:
                #generate JWT
                jwt_key = jwt.encode({"user_id": user[0].id, "email": user[0].email}, "auth_s3cr3t_S1", algorithm="HS256")
            else:
                raise Exception()
        except json.decoder.JSONDecodeError:
            return HttpResponse(json.dumps({"success":False, "error_msg": "Missing POST data"}), content_type="application/json")
        except KeyError as e:
            return HttpResponse(json.dumps({"success":False, "error_msg": "Missing key: " + str(e)}), content_type="application/json")
        except Exception:
            return HttpResponse(json.dumps({"success":False, "error_msg": "Authentication Failed"}), content_type="application/json")
        except:
            return HttpResponse(json.dumps({"success":False}), content_type="application/json")
        else:
            return HttpResponse(json.dumps({"success":True, "auth_key":jwt_key.decode('utf-8')}), content_type="application/json")

class UserHandler(View):
    def send_activation_email(self, email):
        #generate JWT for email activation
        activation_key = jwt.encode({'email': email}, "s3cr3t_key_S1", algorithm="HS256")
        message_body = "Thank you for creating a new account. To use the full range of services of our website you will need to verify the email address for your account. You can do so by clicking the link provided below. \n \n http://<BASE_URL>/verify?key=" + activation_key.decode('utf-8') + " \n \n Thank you very much"
        """#send activation email using gmail creds
        smtp_conn = smtplib.SMTP("smtp.gmail.com:587")
        smtp_conn.ehlo()
        smtp_conn.starttls()
        #gmail creds
        gmail = ""
        gmail_password = ""
        smtp_conn.login(gmail,gmail_password)
        msg = MIMEText(message_body)
        msg["to"] = email
        msg["from"] = gmail
        msg["subject"] = "S1 Registration"
        smtp_conn.send_message(msg)
        smtp_conn.close()"""
        print (message_body)
    
    def post(self, request):
        try:
            #get JSON POST data
            json_data = json.loads(request.body.decode("utf-8"))
            email = json_data["email"]
            password = json_data["password"]
            #check for firstname and lastname
            if "firstname" in json_data:
                firstname = json_data["firstname"]
            else:
                firstname = None
            if "lastname" in json_data:
                lastname = json_data["lastname"]
            else:
                lastname = None
            #create new user
            new_user = Users()
            new_user.email = email
            new_user.password = password
            new_user.firstname = firstname
            new_user.lastname = lastname
            #send activation email
            self.send_activation_email(email)
        except json.decoder.JSONDecodeError:
            return HttpResponse(json.dumps({"success":False, "error_msg": "Missing POST data"}), content_type="application/json")
        except smtplib.SMTPAuthenticationError:
            return HttpResponse(json.dumps({"success":False, "error_msg": "Failed to send activation email"}), content_type="application/json")
        except KeyError as e:
            return HttpResponse(json.dumps({"success":False, "error_msg": "Missing key: " + str(e)}), content_type="application/json")
        except:
            traceback.print_exc()
            return HttpResponse(json.dumps({"success":False}), content_type="application/json")
        else:
            new_user.save()
            return_data = {
                "user_id": new_user.id,
                "email": new_user.email,
                "firstname": new_user.firstname,
                "lastname": new_user.lastname
            }
            return HttpResponse(json.dumps({"success":True, "data": return_data}), content_type="application/json")
    
    def get(self, request):
        try:
            auth_key = request.GET.get("key")    
            users = Users.objects.all()
            user_list = []
            for user in users:
                if auth_key:
                    #decrypt JWT
                    key_data = jwt.decode(auth_key, "auth_s3cr3t_S1")
                    user_list.append({
                    "user_id": user.id,
                    "email": user.email,
                    "firstname": user.firstname,
                    "lastname": user.lastname,
                    "confirmation": user.confirmation.strftime("%Y-%m-%d %H:%M:%S") if user.confirmation else None,
                    })
                else:
                    user_list.append({
                    "user_id": user.id,
                    "firstname": user.firstname,
                    })
            
                
        except jwt.exceptions.DecodeError:
            return HttpResponse(json.dumps({"success":False, "error_msg": "Invalid AuthKey"}), content_type="application/json")
        except:
            traceback.print_exc()
            return HttpResponse(json.dumps({"success":False}), content_type="application/json")
        else:
            return HttpResponse(json.dumps({"success":True, "data": user_list}), content_type="application/json")

class ChangePasswordHandler(View):
    def patch(self, request):
        try:
            #get JSON POST data
            json_data = json.loads(request.body.decode("utf-8"))
            old_password = json_data["old_password"]
            new_password = json_data["new_password"]
            #get POST headers
            key = request.META["HTTP_KEY"]
            #decrypt key
            key_data = jwt.decode(key, "auth_s3cr3t_S1")
            user_id = key_data["user_id"]
            #check old password in DB
            user = Users.objects.get(pk=user_id)
            if old_password == user.password:
                user.password = new_password
            else:
                raise Exception()
        except json.decoder.JSONDecodeError:
            return HttpResponse(json.dumps({"success":False, "error_msg": "Missing POST data"}), content_type="application/json")
        except jwt.exceptions.DecodeError:
            return HttpResponse(json.dumps({"success":False, "error_msg": "Invalid AuthKey"}), content_type="application/json")
        except KeyError as e:
            return HttpResponse(json.dumps({"success":False, "error_msg": "Missing key: " + str(e)}), content_type="application/json")
        except Exception:
            return HttpResponse(json.dumps({"success":False, "error_msg": "Invalid old password"}), content_type="application/json")
        except:
            return HttpResponse(json.dumps({"success":False}), content_type="application/json")
        else:
            user.save()
            return HttpResponse(json.dumps({"success":True}), content_type="application/json")
